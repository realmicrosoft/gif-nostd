pub struct GifHeader {
    #[feature(verify_little_endian)]
    pub signature: [u8; 3],
    #[feature(check_gif_version)]
    pub version: [u8; 3],
}
